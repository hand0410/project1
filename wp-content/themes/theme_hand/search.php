<?php get_header() ?>
	<?php
		$search_query = new WP_Query ('s=' .$s. '&showpost=-1');
		$search_keyword = wp_specialchars( $s, 1);
		$search_count = $search_query->post_count;
		//print_r($search_query);
		//$search_query->title
	?> 
<div id="about" class="container-fluid">
    <div class="row">
    <h4><?php printf(__('Có %1$s kết quả được tìm thấy', 'hand'), $search_count); ?></h4>
    <?php if($wp_query->have_posts()) : while ($wp_query->have_posts ()) : $wp_query->the_post(); ?>
        <div class="col-sm-8">
        <h4><a href="<?php the_permalink () ?>" title="<?php the_title() ?>"><?php the_title () ?></a></h4><br>
        <div class="info-post">
            <span>Post by: <?php the_author(); ?></span> - <span> <?php echo get_the_date (); ?></span>
        </div>      
        <p><?php (!is_single()) ? the_excerpt() : the_content () ?></p>
        
        </div>
    <?php endwhile; ?>
    <?php endif; ?>
    </div>
    </div>
<?php get_footer() ?>