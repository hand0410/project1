<?php 
    /**Teamplate Name: Contact */
?>
<?php get_header () ?>
<div class="content">
    <div id="main-content">
        <div class="contact-info">
            <h4>Địa chỉ liên hệ</h4>
            <p>Nguyễn Đình Hà, Xóm 5, Thanh Thịnh, Thanh Chương, Nghệ An</p>
            <p>0981235548</p>
        </div>
        <div class="contact-info">
            <?php echo do_shortcode ('[contact-form-7 id="120" title="Contact form 1"]'); ?>
        </div>
    </div>
</div>