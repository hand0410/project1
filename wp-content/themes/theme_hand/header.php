<!DOCTYPE html>
<html lang="<?php language_attributes (); ?>">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Bootstrap Theme Company Page</title>
  <meta charset="<?bloginfo ('charset') ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <!-- <link href="http://127.0.0.1/wp/wp-content/themes/theme_hand/css/style.css" rel="stylesheet" type="text/css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="http://127.0.0.1/wp/wp-content/themes/theme_hand/js/common.js"></script>
  <link rel="profile" href="http://gmgp.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <?php wp_head() ?>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>                        
		</button>
		<a class="navbar-brand" href="#myPage">Logo</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
		<?php hand_menu ('primary-menu');?>
		</div>
	</div>
</nav>
<div class="jumbotron text-center">
	<h1>Company</h1> 
	<p>We specialize in blablabla</p> 
	<form action="http://127.0.0.1/wp/" method="get" id="adminbarsearch" class="">
		<div class="input-group">
		<input class="adminbar-input form-control" name="s" id="adminbar-search" type="text" value="" maxlength="150">
		<div class="input-group-btn">
			<!-- <button type="button" class="btn btn-danger">Search</button> -->
			<button type="submit" class="adminbar-button btn btn-danger" value="Search">Search</button>
		</div>
		</div>
	</form>
</div>