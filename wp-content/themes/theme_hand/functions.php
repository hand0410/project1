<?php
/**Nhung file style.css */
function thachpham_styles() {
    wp_register_style( 'main-style', get_template_directory_uri(). '/style.css', 'all' );
    wp_enqueue_style( 'main-style' );
  }
  add_action( 'wp_enqueue_scripts', 'thachpham_styles' );

if(! function_exists ('theme_setup')){
    function theme_setup (){
        /**Them menu */
        register_nav_menu('primary-menu', __('Primary Menu', 'Hand'));
        /**Them tag */
        add_theme_support ('title-tag');
    }
    add_action ('init', 'theme_setup');
}
if (!function_exists('hand_menu')){
    function hand_menu ($menu){
        $menu = array (
            'theme_location'=>$menu,
            'container'=>'ul',
            'container_class'=>'test',
            'menu_class' => "nav navbar-nav navbar-right"
        );
        wp_nav_menu ($menu);
    }
}
//Show tag
if (!function_exists('show_tag')){
    function show_tag(){
        if(has_tag()){
            printf(__('Tagged in: %1$s', 'hand'), get_the_tag_list ('',','));
        }
    }
}
function cut_string ($str, $value){
    $str = substr("$str", 0, $value) . "...";
    return $str;
}
function view_post_number ($number){
    $count = 0;
    if( have_posts()){
        while ( have_posts() && $count < $number) {
            the_post();
            $count++;
            printf('<h5><a href="%1$s">%2$s</a></h5>', get_the_permalink(), get_the_title ());
        }
    }else{
        return false;
    }
}
//select post by ID
function select_post_by_ca ($category, $number = false){
    $total = 0;
    $args = array(
        'cat' => $category,
        'post_type' => 'post'
    );
    $post_list = get_posts($args);
    foreach ($post_list as $post) {
        if($number){
            if($number && $total < $number){
                $total++;
                setup_postdata( $post );
                printf('<h5><a href="%1$s">%2$s</a></h5>', get_the_permalink($post), get_the_title ($post));
            }
        }else{
            setup_postdata( $post );
            printf('<h5><a href="%1$s">%2$s</a></h5>', get_the_permalink($post), get_the_title ($post));
        }
    }
}
?>