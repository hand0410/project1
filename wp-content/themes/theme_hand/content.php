<div id="about" class="container-fluid">
    <div class="row">
        <div class="col-sm-8">
            <?php if(have_posts()) : while (have_posts ()) : the_post(); 
            //global $post;
            /*echo "<pre>";
            print_r($post);
            echo "</pre>";*/
            ?>
            <div class="col-sm-12">
                <h4><a href="<?php the_permalink () ?>" title="<?php the_title() ?>"><?php the_title () ?></a></h4><br>
                <div class="info-post">
                    <span>Post by: <?php the_author(); ?></span> - <span> <?php echo get_the_date (); ?></span>
                </div>      
                <p><?php (!is_single()) ? the_excerpt() : the_content () ?></p>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    <div class="col-sm-4">
        <?php foreach (get_categories() as $key => $value) {
            ?>
        <div class="col-sm-12 post-right">
            <h4 class=""><a href="<?php echo get_category_link ($value->cat_ID)?>"><?php echo $value->name; ?></a></h4>
            <div class="col-sm-4">
            <?php
                $args = array( 'category' => $value->cat_ID, 'post_type' =>  'post' );
                $postslist = get_posts( $args );
                foreach ($postslist as $post) :  setup_postdata($post);
            ?> 
                <img src="<?php echo (!empty(wp_get_attachment_url(get_post_thumbnail_id( $post->ID ))) ? wp_get_attachment_url(get_post_thumbnail_id( $post->ID )) : get_template_directory_uri() . '/images/noimage.jpg'); ?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
            <?php
                break;
                endforeach;
            ?>
            </div>
            <div class="col-sm-8">
            <?php
                $args = array( 'category' => $value->cat_ID, 'post_type' =>  'post' );
                $postslist = get_posts( $args );
                foreach ($postslist as $post) :  setup_postdata($post);
            ?>  
                <h4><a href="<? the_permalink () ?>"><?php the_title(); ?></a></h4>
                <p><?php echo cut_string (get_the_excerpt(), 160) ?></p>
            <?php
                break;
                endforeach;
            ?>
            </div>
            <div class="col-sm-12">
                <?php select_post_by_ca ($value->cat_ID, 3); ?>
            </div>
        </div>
        <?php } ?>
    </div>
    </div>
    </div>
    <h5><?php if ( function_exists( 'pgntn_display_pagination' ) ) pgntn_display_pagination( 'posts' ); ?></h5>
                
    <div id="contact" class="container-fluid bg-grey">
    <h2 class="text-center">CONTACT</h2>
    <div class="row">
        <div class="col-sm-5">
        <p>Contact us and we'll get back to you within 24 hours.</p>
        <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
        <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
        <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
        </div>
        <div class="col-sm-7 slideanim">
        <div class="row">
            <div class="col-sm-6 form-group">
            <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
            </div>
            <div class="col-sm-6 form-group">
            <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
            </div>
        </div>
        <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
        <div class="row">
            <div class="col-sm-12 form-group">
            <button class="btn btn-default pull-right" type="submit">Send</button>
            </div>
        </div>
        </div>
    </div>
    </div>