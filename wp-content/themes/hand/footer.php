<footer class="footer">
    <div class="copyright">
        &copy; - <?php echo date('y');?> - <?php bloginfo('sitename');?>
    </div>
</footer>
</div> <!--end container-->
<?php wp_footer() ;?>
</body>
</html>