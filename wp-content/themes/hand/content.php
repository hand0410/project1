<article id="post-<?php the_ID();?>" <?php post_class(); ?>>
    <div class="entry-thumbnail">
        <?php hand_thumbnail('thumbnail');?>
    </div>
    <div class="entry-header">
        <?php hand_entry_header (); ?>
        <?php hand_entry_meta () ?>
    </div>
    <div class="entry-content">
        <?php hand_entry_content () ?>
        <?php (is_single() ? show_tag() : '') ?>
    </div>
</article>