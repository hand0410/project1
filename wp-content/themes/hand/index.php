<?php get_header(); ?>
<div class="content">
    <div class="main-content">
<?php if( have_posts ()) : while (have_posts()) : the_post(); ?>
    <?php the_title () ?></br>

    <?php endwhile?>
    <?php hand_pagination();?>
    <?php else: ?>
    <?php get_template_part('content','none');?>
<?php endif;?>
    </div>
    <div class="sidebar">
        <?php get_sidebar();?>
    </div>
</div>
<?php get_footer(); ?>