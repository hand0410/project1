<?php
/**
 @ Khai bao hang gia tri
    @ THEME_URL = lay duong dan thu muc theme
    @ CORE  = lay duong dan cua thu muc /core
 */
define ('THEME_URL', get_stylesheet_directory());
define ('CORE',THEME_URL."/core");
/**
 @ nhung file /core/init.php
 */
require_once(CORE."/init.php");
/**
 @ Thiet lap chieu rong noi dung
 */
if(!isset($content_width)){
    $content_width = 620;
}
/**
 @ Khai bao chuc nang cua theme
 
 */
if( !function_exists('theme_setup')){
    function theme_setup (){
        /*Thiet lap tex domain */
        $language_foder = THEME_URL . '/languages';
        //print_r ($language_foder);
        load_theme_textdomain('hand', $language_foder);
        /*Tu dong them linh RSS*/
        add_theme_support('automatic-feed-links');
        /**Them post thumbnail*/
        add_theme_support ('post-thumbnails');
        /**Them post format */
        add_theme_support('post-formats', array(
            'image',
            'video',
            'gallery',
            'link'
        ));
        /**Them title tag */
        add_theme_support ('title-tag');
        /**Them custom bacground */
        $default_bacground = array(
            'default_background' => '#e8e8e8'
        );
        add_theme_support('custom-background');
        /**Them menu */
        register_nav_menu('tets', __('Primary Menu', 'Hand'));
        /**Tao slidebar */
        $sidebar = array(
            'name' => __('Main Sidevar', 'hand'),
            'id' => 'main-sidebar',
            'before_title'=>'<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
    }
    add_action ('init', 'theme_setup');
}
//TEMPLATE HEADER
if ( !function_exists('hand_header')){
    function hand_header (){ ?>
        <div class="site-name">
        <?php
        if( is_home()){
            printf ('<h1><a href="%1$s" title="%2$s">%3$s</a></h1>',
            get_bloginfo('url'),
            get_bloginfo('description'), 
            get_bloginfo('sitename') );
        }else{
            printf ('<p><a href="%1$s" title="%2$s">%3$s</a></p>',
            get_bloginfo('url'),
            get_bloginfo('description'), 
            get_bloginfo('sitename') );
        }
        ?>
        </div>
        <div class="site-description"><?php bloginfo('description') ?></div><?php
    }
}
/**Thiet lap menu */
if (!function_exists('hand_menu')){
    function hand_menu ($menu){
        $menu = array (
            'theme_location'=>$menu,
            'container'=>'nav',
            'container_class'=>$menu
        );
        wp_nav_menu ($menu);
    }
}
/**Ham tao phan trang don gian */
if( !function_exists('hand_pagination')){
    function hand_pagination (){
        if($GLOBALS['wp_query']->max_num_pages <2){
            return '';
        } ?>
        <nav class="pagination" role="navigation">
            <?php if (get_next_post_link()) : ?>
            <div class="prev"><?php next_posts_link(__('Oler Posts', 'hand'));?></div>
            <?php endif; ?>
            <?php if (get_previous_posts_link()) : ?>
            <div class="next"><?php previous_posts_link(__('Newest Posts', 'hand')); ?></div>
    <?php endif; ?>
    </nav>
    <?php
    }
}
/**Ham hien thi thumbnail */
if( !function_exists ('hand_thumbnail')){
    function hand_thumbnail($size){
        if( !is_single() && has_post_thumbnail() && !post_password_required() || has_post_format('image')) :?>
        <figure class="post-thumbnail"><?php the_post_thumbnail($size); ?></figure>
    <?php endif;?>
    <?php }
}
/**Hien thi tieu de post */
if( !function_exists('hand_entry_header')){
    function hand_entry_header () {?>
        <?php if(is_single ()) : ?>
        <h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><?php the_title(); ?></a></h1>
        <?php else : ?>
        <h2 class="entry-title"><a href="<?php the_permalink();?>" title="<?php the_title() ?>"><?php the_title(); ?></a></h2>
    <?php endif; ?>
    <?php }
}
/**Lay dữ liệu post */
if (!function_exists('hand_entry_meta')){
    function hand_entry_meta(){ ?>
        <?php if(!is_page()) :?>
    <div class="entry-meta">
    <?php
        printf(__('<span class="author">Posted by %1$s', 'hand'), get_the_author ());
        printf(__('<span class="date"> at %1$s', 'hand'), get_the_date());
        printf(__('<span class="category"> in %1$s', 'hand'), get_the_category_list(','));
        if(comments_open()):
        echo '<span class="meta-reply">';
            comments_popup_link(
                __('Leave a comment','hand'),
                __('One comment', 'hand'),
                __('%comment','hand'),
                __('Read all comments','hand')
            );
        echo '</span>';
        endif;
    ?>
    </div>
    <?php endif; ?>
    <?php }
}
/**Lay noi dung cua bai viet */
if (!function_exists ('hand_entry_content')){
    function hand_entry_content (){
        if(!is_single ()){
            the_excerpt ();
        }else{
            the_content();
        }
    }
}
if (!function_exists ('Read_more')){
    function Read_more (){
        return '<a class="read-more" href="'.get_permalink(get_the_ID()).'">'.__('...[Read more]', 'hand').'</a>';
    }
    add_filter('excerpt_more', 'Read_more');
}
if(!function_exists('show_tag')){
    function show_tag (){
        if(has_tag()):
            echo '<div class="entry-tag"';
            printf(__('Tagged in %1$s', 'hand'), get_the_tag_list(' ',','));
            echo '</div>';
        endif;
    }
}
?>